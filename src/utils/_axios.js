import axios from 'axios'

const token = import.meta.env.VITE_APP_GITHUB_PERSONEL_ACCESS_TOKEN

const instance = axios.create({
  baseURL: 'https://api.github.com',
  headers: {
    'Authorization': token ? `Bearer ${token}` : undefined,
  },
})

export default instance
