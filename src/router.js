import { createRouter, createWebHistory } from 'vue-router'

import PageHome from './pages/home/Index.vue'

const routes = [
  {
    path: '/',
    component: PageHome,
  },

  {
    path: '/profile/:username',
    component: () => import('./pages/profile/Username.vue'),
    props: (route) => ({
      username: route.params.username,
    }),
  },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
})

export default router
