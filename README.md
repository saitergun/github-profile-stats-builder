# Github Profile Stats Builder

## Kurulum

---

#### Projeyi bilgisayara indir:

```bash
git clone https://gitlab.com/saitergun/github-profile-stats-builder
```

#### Bağımlılıkları yükle:

```bash
yarn install
```

#### Environment (.env) dosyasını güncelle:

> Github tokensız istekler için rate limit koymuş.
> API isteklerini token ile atmak için .env dosyasına token'ı eklememiz gerekiyor.
> Token'ı Developer Settings sayfasın altındaki Personel Access Token (https://github.com/settings/tokens?type=beta) sayfasından alabiliyoruz.

```bash
VITE_APP_GITHUB_PERSONEL_ACCESS_TOKEN=""
```

#### Projeyi ayağa kaldır:

```bash
yarn dev
```

#### Uygulamayı ziyaret et:

```
https://localhost:5173
```

#### Kullanıcı istatistiklerine ulaşmak için:

1. Anasayfadaki arama kutusuna kullanıcı adı yaz.
2. Arama sonuçlarından gelen kullanıcılardan herhangi birine tıkla.
3. İstatistikleri bilgisayara png olarak kaydetmek için "Take a screenshot" butonuna tıkla.

#### Build al:

```bash
yarn build
```
